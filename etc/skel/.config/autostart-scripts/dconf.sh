#!/bin/sh
dconf load / < ~/.config/croco-dconf.ini
rm -rf ~/.config/croco-dconf.ini &
rm -rf ~/.config/autostart-scripts/dconf.sh &
 
notify-send "GNOME settings applied! 🔥"
